﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace program
{
    class program
    {
        static void date()
        {
            var today = DateTime.Today;

            Console.WriteLine("Please enter your date of birth: (mm-dd-yyyy)");
            var dob = Console.ReadLine();

            DateTime birthDate = DateTime.Parse(dob);
            DateTime current = DateTime.Today;

            TimeSpan elapsed = today.Subtract(birthDate);

            double daysAgo = elapsed.TotalDays;
            Console.WriteLine($"You're {daysAgo} old.");
        }
        static void score()
        {

        }

        static void random()
        {
            int score = 0;
            int i = 0;


            do
            {

                int number = 0;

                Random rnd = new Random();
                int Value = rnd.Next(1, 6);

                Console.WriteLine("--------------------------------");
                Console.WriteLine($"Your Current Score Is {score}");
                Console.WriteLine("--------------------------------");
                Console.WriteLine("Please guess a number between 1-5");

                number = int.Parse(Console.ReadLine());
                Console.WriteLine($"You entered {number} and the number was {Value}");

                Console.Clear();

                if
                    (number == Value)
                    score++;


                i++;
            } while (i <= 4);



            Console.Clear();
            Console.WriteLine($"Well done you made it to the end and you got {score} out of 5 ");
        }

         
        static void Main(string[] args)
        {
            var ansMenu = 0;
            var returnMenu = "";
            
            do
            {
                Console.WriteLine("-- menu   -----");
                Console.WriteLine("1. opt1   -----");
                Console.WriteLine("2. opt2   -----");
                Console.WriteLine("3. opt3   -----");
                Console.WriteLine("4. opt4   -----");
                Console.WriteLine("---------------");
                Console.WriteLine();
                Console.WriteLine("Please enter a selection: ");
                ansMenu = int.Parse(Console.ReadLine());
                Console.Clear();

                switch (ansMenu)
                {
                    case 1:
                        date();
                        Console.WriteLine("Press [m/M] to return to menu..");
                        returnMenu = Console.ReadLine();
                        Console.Clear();
                        break;
                    case 2:
                        Console.WriteLine("Press [m/M] to return to menu..");
                        returnMenu = Console.ReadLine();
                        Console.Clear();
                        break;
                    case 3:
                        random();
                        Console.WriteLine("Press [m/M] to return to menu..");
                        returnMenu = Console.ReadLine();
                        Console.Clear();
                        break;
                    case 4:
                        foodMenu();                        
                        Console.WriteLine("Press [m/M] to return to menu..");
                        returnMenu = Console.ReadLine();
                        Console.Clear();
                        break;
                    default:
                        Console.WriteLine("Press [m/M] to return to menu..");
                        returnMenu = Console.ReadLine();
                        Console.Clear();
                        break;
                }

            } while (returnMenu == "m" || returnMenu == "M");
        }

        static void foodMenu()
        {
            var submenu = "";

            do

            {
                
                
                Console.Clear();
                Console.WriteLine("      Food menu      ");
                Console.WriteLine("                     ");
                Console.WriteLine("-- 1. Enter foods  --");
                Console.WriteLine("-- 2. Show list    --");
                Console.WriteLine("-- 3. change       --");
                Console.WriteLine("-- 4. End          --\n");
                Console.WriteLine();
                Console.WriteLine("Please select a number");



                String input = Console.ReadLine();
                int selectedOption;
                
                if (int.TryParse(input, out selectedOption))


                {
                    switch (selectedOption)
                    {
                        case 1:
                            foods();
                            Console.WriteLine("Press r/R for to return to sub menu or press Enter for main menu selection");
                            submenu = Console.ReadLine();
                            break;
                        case 2:
                            show(food);
                            Console.WriteLine("Press r/R for to return to sub menu or press Enter for main menu selection");
                            submenu = Console.ReadLine();
                            break;
                        case 3:
                            change();
                            Console.WriteLine("Press r/R for to return to sub menu or press Enter for main menu selection");
                            submenu = Console.ReadLine();
                            break;
                        case 4:
                            end();
                            Console.WriteLine("Press r/R for to return to sub menu or press Enter for main menu selection");
                            submenu = Console.ReadLine();
                            break;
                        default:
                            Console.WriteLine("Press r/R for to return to sub menu or press Enter for main menu selection");
                            submenu = Console.ReadLine();
                            break;
                    }

                }else
                {

                }

            } while (submenu == "r" || submenu == "R");

                   

}

        static Dictionary<int, string> food = new Dictionary<int, string>();

        static Dictionary<int, string> foods()
        {
            

            Console.WriteLine("Please enter 5 of your favorite foods in order of favorite to least favorite. (Type stop to finish)\n");

            string input = "";
            int value = 0;


            while (!input.Equals("stop"))
            {
                input = Console.ReadLine();
                value++;


                if (input.Equals("stop"))

                {
                    break;
                }


                food.Add(value, input);

            }

            return food;
        }

        static void show(Dictionary<int,string> food)
        {
            Console.WriteLine("Your favorite foods in order are:  \n");

            foreach (KeyValuePair<int, string> f in food)
            {
                Console.WriteLine(f);
            }
        }
        
        static void change()
        {
            var fooditem = 0;

            show(food);

            Console.WriteLine("What item of food would you like to remove?");
            fooditem = int.Parse(Console.ReadLine());

            if (fooditem == 1)
            {
                food.Remove(1);
            }
            if (fooditem == 2)
            {
                food.Remove(2);
            }
            if (fooditem == 3)
            {
                food.Remove(3);
            }
            if (fooditem == 4)
            {
                food.Remove(4);
            }
            if (fooditem == 5)
            {
                food.Remove(5);
            }

            show(food);
        }
        static void end()
        {
            Console.WriteLine("Thanks for taking part\n");
        }
    }
}